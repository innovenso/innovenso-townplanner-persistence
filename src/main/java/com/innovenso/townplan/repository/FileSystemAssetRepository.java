package com.innovenso.townplan.repository;

import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
public class FileSystemAssetRepository implements AssetRepository {
	private final File targetBaseDirectory;
	private final String cdnBaseUrl;

	public FileSystemAssetRepository(@NonNull final String targetBasePath, @NonNull final String cdnBaseUrl) {
		this.targetBaseDirectory = new File(targetBasePath);
		this.targetBaseDirectory.mkdirs();
		this.cdnBaseUrl = cdnBaseUrl;
	}

	@Override
	public void write(File file, String objectName) {
		try {
			FileUtils.copyFile(file, getPath(objectName).toFile());
		} catch (IOException e) {
			log.error(e);
		}
	}

	public Path getPath(@NonNull final String objectName) {
		return Paths.get(targetBaseDirectory.getAbsolutePath(), objectName);
	}

	@Override
	public Optional<File> read(String objectName) {
		final File file = getPath(objectName).toFile();
		if (file.canRead())
			return Optional.of(file);
		return Optional.empty();
	}

	@Override
	public List<String> getObjectNames() {
		try (Stream<Path> walk = Files.walk(targetBaseDirectory.toPath())) {

			return walk.filter(Files::isRegularFile).map(Path::toString)
					.map(key -> StringUtils.removeStart(key, targetBaseDirectory.getAbsolutePath() + "/"))
					.collect(Collectors.toList());

		} catch (IOException e) {
			log.error(e);
			return List.of();
		}
	}

	@Override
	public Optional<String> getCdnUrl(String objectName) {
		return Optional.of(cdnBaseUrl + objectName);
	}
}
