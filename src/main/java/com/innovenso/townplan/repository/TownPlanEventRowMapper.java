package com.innovenso.townplan.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovenso.townplan.api.event.TownPlanEvent;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.NonNull;

import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j2
public class TownPlanEventRowMapper implements RowMapper<TownPlanEvent> {
	private final ObjectMapper objectMapper;

	public TownPlanEventRowMapper(@NonNull final ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	@Override
	public TownPlanEvent mapRow(ResultSet rs, int rowNum) throws SQLException {
		try {
			log.debug("event body: {}", rs.getString("event_body"));
			return objectMapper.readValue(rs.getString("event_body"), TownPlanEvent.class);
		} catch (JsonProcessingException e) {
			throw new SQLException("could not read event from row body");
		}
	}
}
