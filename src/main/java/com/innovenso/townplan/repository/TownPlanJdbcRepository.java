package com.innovenso.townplan.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.innovenso.eventsourcing.api.exception.EntityChangeVetoException;
import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.domain.entity.EntityHistory;
import com.innovenso.townplan.api.event.TownPlanEvent;
import com.innovenso.townplan.broadcast.TownPlanBroadcaster;
import com.innovenso.townplan.domain.TownPlanImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.lang.NonNull;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;

@Log4j2
public class TownPlanJdbcRepository implements TownPlanRepository {
	private final JdbcTemplate jdbcTemplate;
	private final TownPlanBroadcaster broadcaster;
	private final ObjectMapperFactory objectMapperFactory;

	public TownPlanJdbcRepository(@NonNull final JdbcTemplate jdbcTemplate,
			@NonNull final TownPlanBroadcaster broadcaster) {
		this.jdbcTemplate = jdbcTemplate;
		this.broadcaster = broadcaster;
		this.objectMapperFactory = new ObjectMapperFactory();
	}

	@Override
	public EntityHistory<TownPlanEvent> getEntityHistory(EntityId entityId) {
		log.debug("loading entity history for entity " + entityId);
		final EntityHistory<TownPlanEvent> entityHistory = new EntityHistory<>(entityId);
		final String query = "SELECT event_body FROM events WHERE entity_id = ?";
		jdbcTemplate
				.queryForStream(query, new TownPlanEventRowMapper(this.objectMapperFactory.getObjectMapper()),
						entityId.value)
				.peek(event -> log.debug("loading event {}", event)).forEach(entityHistory::addEvent);
		return entityHistory;
	}

	@Override
	public List<EntityId> listEntities() {
		jdbcTemplate.query("SELECT DISTINCT entity_id FROM events", new EntityIdRowMapper());
		return null;
	}

	@Override
	@Transactional
	public TownPlanImpl reset(EntityId entityId) {
		jdbcTemplate.update("DELETE FROM events WHERE entity_id = ?", entityId.value);
		return getLatestEntityRevision(entityId);
	}

	@Override
	public void setRepositoryRoot(@lombok.NonNull File repositoryRoot) {
	}

	@Override
	@Transactional
	public void onChange(TownPlanEvent townPlanEvent) throws EntityChangeVetoException {
		try {
			jdbcTemplate.update("INSERT INTO events(entity_id, event_id, event_type, event_body) VALUES (?,?,?,?)",
					townPlanEvent.getId().getEntityId().value, townPlanEvent.getId().getNewRevisionId().value,
					objectMapperFactory.getEventTypes().getOrDefault(townPlanEvent.getClass(), "UnknownEvent"),
					this.objectMapperFactory.getObjectMapper().writeValueAsString(townPlanEvent));
		} catch (JsonProcessingException e) {
			throw new EntityChangeVetoException("could not serialize event to string: " + townPlanEvent, e);
		} catch (Exception e) {
			throw new EntityChangeVetoException("could not write event to database", e);
		}
		try {
			broadcaster.onEvent(townPlanEvent);
		} catch (Throwable e) {
			log.error("Could not broadcast event", e);
		}
	}
}
