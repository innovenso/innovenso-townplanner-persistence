package com.innovenso.townplan.repository;

import com.innovenso.eventsourcing.api.id.EntityId;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EntityIdRowMapper implements RowMapper<EntityId> {
	@Override
	public EntityId mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new EntityId(rs.getString("entity_id"));
	}
}
