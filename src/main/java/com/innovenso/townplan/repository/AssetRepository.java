package com.innovenso.townplan.repository;

import java.io.File;
import java.util.List;
import java.util.Optional;

public interface AssetRepository {
	void write(File file, String objectName);

	Optional<File> read(String objectName);

	List<String> getObjectNames();

	Optional<String> getCdnUrl(String objectName);
}
