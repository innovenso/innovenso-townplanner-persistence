package com.innovenso.townplan.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.innovenso.townplan.api.event.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ObjectMapperFactory {
	public ObjectMapper getObjectMapper() {
		return ((JsonMapper.Builder) JsonMapper.builder().registerSubtypes(this.getNamedTypes()))
				.addModule(new JavaTimeModule()).build();
	}

	public NamedType[] getNamedTypes() {
		return (NamedType[]) ((List) this.getEventTypes().entrySet().stream().map((entry) -> {
			return new NamedType((Class) entry.getKey(), (String) entry.getValue());
		}).collect(Collectors.toList())).toArray(new NamedType[0]);
	}

	public Map<Class<? extends TownPlanEvent>, String> getEventTypes() {
		Map<Class<? extends TownPlanEvent>, String> map = new HashMap<>();
		map.put(EnterpriseAddedEvent.class, "EnterpriseAdded");
		map.put(CapabilityAddedEvent.class, "CapabilityAdded");
		map.put(BuildingBlockAddedEvent.class, "BuildingBlockAdded");
		map.put(ActorAddedEvent.class, "ActorAdded");
		map.put(TechnologyAddedEvent.class, "TechnologyAdded");
		map.put(ITSystemAddedEvent.class, "ITSystemAdded");
		map.put(ITContainerAddedEvent.class, "ITContainerAdded");
		map.put(DataEntityAddedEvent.class, "DataEntityAdded");
		map.put(ITContainerTechnologyAddedEvent.class, "ITContainerTechnologyAdded");
		map.put(RelationshipAddedEvent.class, "RelationshipAdded");
		map.put(FlowViewAddedEvent.class, "FlowViewAdded");
		map.put(FlowViewStepAddedEvent.class, "FlowViewStepAdded");
		map.put(FlowViewStepsClearedEvent.class, "FlowViewStepsCleared");
		map.put(FlowViewStepsSetEvent.class, "FlowViewStepsSet");
		map.put(SwotAspectAddedEvent.class, "SwotAspectAdded");
		map.put(LifecycleAspectSetEvent.class, "LifecycleAspectSet");
		map.put(DocumentationAspectAddedEvent.class, "DocumentationAspectAdded");
		map.put(ImpactViewAddedEvent.class, "ImpactViewAdded");
		map.put(ImpactViewCapabilityAddedEvent.class, "ImpactViewCapabilityAdded");
		map.put(AwsRegionAddedEvent.class, "AwsRegionAdded");
		map.put(AwsVpcAddedEvent.class, "AwsVpcAdded");
		map.put(AwsAvailabilityZoneAddedEvent.class, "AwsAvailabilityZoneAdded");
		map.put(AwsSubnetAddedEvent.class, "AwsSubnetAdded");
		map.put(AwsNetworkAclAddedEvent.class, "AwsNetworkAclAdded");
		map.put(AwsRouteTableAddedEvent.class, "AwsRouteTableAdded");
		map.put(AwsSecurityGroupAddedEvent.class, "AwsSecurityGroupAdded");
		map.put(AwsInstanceAddedEvent.class, "AwsInstanceAdded");
		map.put(SecurityGroupAssignedToInstanceEvent.class, "SecurityGroupAssignedToInstance");
		map.put(AwsAccountAddedEvent.class, "AwsAccountAdded");
		map.put(SubnetAssignedToInstanceEvent.class, "SubnetAssignedToInstance");
		map.put(AwsAutoscalingGroupAddedEvent.class, "AwsAutoscalingGroupAdded");
		map.put(AvailabilityZoneAssignedToAutoScalingGroupEvent.class, "AZAssignedToAutoScaling");
		map.put(SubnetAssignedToAutoScalingGroupEvent.class, "SubnetAssignedToAutoScaling");
		map.put(InstanceAssignedToAutoScalingGroupEvent.class, "InstanceAssignedToAutoScaling");
		map.put(AwsElasticIpAddressAddedEvent.class, "AwsElasticIpAddressAdded");
		map.put(AwsElementClearedEvent.class, "AwsCleared");
		map.put(ItProjectAddedEvent.class, "ItProjectAdded");
		map.put(ITSystemUpdatedEvent.class, "ItSystemUpdated");
		map.put(ITContainerUpdatedEvent.class, "ItContainerUpdated");
		map.put(RelationshipDeletedEvent.class, "RelationshipDeleted");
		map.put(ActorUpdatedEvent.class, "ActorUpdated");
		map.put(BuildingBlockUpdatedEvent.class, "BuildingBlockUpdated");
		map.put(CapabilityUpdatedEvent.class, "CapabilityUpdated");
		map.put(EnterpriseUpdatedEvent.class, "EnterpriseUpdated");
		map.put(ItProjectUpdatedEvent.class, "ProjectUpdated");
		map.put(RelationshipUpdatedEvent.class, "RelationshipUpdated");
		map.put(ItProjectMilestoneAddedEvent.class, "ProjectMilestoneAdded");
		map.put(ItProjectMilestoneUpdatedEvent.class, "ProjectMilestoneUpdated");
		map.put(FlowViewDeletedEvent.class, "FlowViewDeleted");
		map.put(FlowViewUpdatedEvent.class, "FlowViewUpdated");
		map.put(RelationshipTechnologyAddedEvent.class, "RelationshipTechnologyAdded");
		map.put(ElementDeletedEvent.class, "ElementDeleted");
		map.put(ITContainerTechnologyDeletedEvent.class, "ContainerTechnologyDeleted");
		map.put(SwotAspectDeletedEvent.class, "SwotAspectDeleted");
		map.put(DocumentationAspectDeletedEvent.class, "DocumentationAspectDeleted");
		map.put(ContentDistributionAspectAddedEvent.class, "ContentDistributionAspectAdded");
		map.put(ITSystemIntegrationAddedEvent.class, "ITSystemIntegrationAdded");
		map.put(ITSystemIntegrationUpdatedEvent.class, "ITSystemIntegrationUpdated");
		map.put(SystemIntegrationStepsSetEvent.class, "ITSystemIntegrationStepsSet");
		map.put(DataEntityUpdatedEvent.class, "DataEntityUpdated");
		map.put(ConstraintAspectAddedEvent.class, "ConstraintAspectAdded");
		map.put(QualityAttributeRequirementAspectAddedEvent.class, "QualityAttributeRequirementAspectAdded");
		map.put(FunctionalRequirementAspectAddedEvent.class, "FunctionalRequirementAspectAdded");
		map.put(SecurityConcernAspectAddedEvent.class, "SecurityConcernAspectAdded");
		map.put(SecurityImpactAspectAddedEvent.class, "SecurityImpactAspectAdded");
		map.put(ExternalIdAspectAddedEvent.class, "ExternalIdAdded");
		map.put(ExternalIdAspectDeletedEvent.class, "ExternalIdDeleted");
		map.put(ArchitectureVerdictAspectSetEvent.class, "ArchitectureVerdictAspectSet");
		map.put(AspectDeletedEvent.class, "AspectDeleted");
		map.put(CostAspectAddedEvent.class, "CostAdded");
		map.put(AttachmentAspectAddedEvent.class, "AttachmentAspectAdded");
		map.put(PrincipleAddedEvent.class, "PrincipleAdded");
		map.put(PrincipleUpdatedEvent.class, "PrincipleUpdated");
		map.put(DecisionAddedEvent.class, "DecisionAdded");
		map.put(DecisionUpdatedEvent.class, "DecisionUpdated");
		map.put(DecisionContextAddedEvent.class, "DecisionContextAdded");
		map.put(DecisionContextUpdatedEvent.class, "DecisionContextUpdated");
		map.put(DecisionOptionAddedEvent.class, "DecisionOptionAdded");
		map.put(DecisionOptionUpdatedEvent.class, "DecisionOptionUpdated");
		map.put(FatherTimeAspectAddedEvent.class, "FatherTimeAspectAdded");
		map.put(KeyPointInTimeAddedEvent.class, "KeyPointInTimeAdded");
		map.put(KeyPointInTimeDeletedEvent.class, "KeyPointInTimeDeleted");
		map.put(ITPlatformAddedEvent.class, "ITPlatformAdded");
		map.put(ITPlatformUpdatedEvent.class, "ITPlatformUpdated");
		map.put(DataEntityFieldAddedEvent.class, "DataEntityFieldAdded");
		map.put(DataEntityFieldUpdatedEvent.class, "DataEntityFieldUpdated");
		map.put(ConstraintScoreAspectAddedEvent.class, "ConstraintScoreAspectAdded");
		map.put(FunctionalRequirementScoreAspectAddedEvent.class, "FunctionalRequirementScoreAspectAdded");
		map.put(QualityAttributeRequirementScoreAspectAddedEvent.class, "QualityAttributeRequirementScoreAdded");
		return map;
	}
}
