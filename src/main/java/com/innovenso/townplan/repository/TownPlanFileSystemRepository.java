package com.innovenso.townplan.repository;

import com.innovenso.eventsourcing.api.exception.EntityChangeVetoException;
import com.innovenso.eventsourcing.infrastructure.repository.EntityDomainFileSystemRepository;
import com.innovenso.townplan.api.command.TownPlanCommand;
import com.innovenso.townplan.api.event.TownPlanEvent;
import com.innovenso.townplan.broadcast.TownPlanBroadcaster;
import com.innovenso.townplan.domain.TownPlanImpl;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;

import java.io.File;
import java.util.Map;

@Log4j2
public class TownPlanFileSystemRepository
		extends
			EntityDomainFileSystemRepository<TownPlanImpl, TownPlanCommand, TownPlanEvent>
		implements
			TownPlanRepository {
	private final TownPlanBroadcaster broadcaster;
	private final ObjectMapperFactory objectMapperFactory;

	public TownPlanFileSystemRepository(@NonNull final String repositoryDirectory,
			@NonNull final TownPlanBroadcaster broadcaster) {
		super(new File(repositoryDirectory), TownPlanImpl.class, TownPlanEvent.class);
		this.broadcaster = broadcaster;
		this.objectMapperFactory = new ObjectMapperFactory();
	}

	@Override
	public void onChange(TownPlanEvent event) throws EntityChangeVetoException {
		super.onChange(event);
		try {
			broadcaster.onEvent(event);
		} catch (Throwable e) {
			log.error("Could not broadcast event", e);
		}
	}

	@Override
	protected Map<Class<? extends TownPlanEvent>, String> getEventTypes() {
		return objectMapperFactory.getEventTypes();
	}
}
