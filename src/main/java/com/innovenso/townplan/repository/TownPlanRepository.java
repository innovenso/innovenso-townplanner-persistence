package com.innovenso.townplan.repository;

import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.domain.repository.EntityDomainRepository;
import com.innovenso.townplan.api.command.TownPlanCommand;
import com.innovenso.townplan.api.event.TownPlanEvent;
import com.innovenso.townplan.domain.TownPlanImpl;
import lombok.NonNull;

import java.io.File;
import java.util.function.Supplier;

public interface TownPlanRepository extends EntityDomainRepository<TownPlanImpl, TownPlanCommand, TownPlanEvent> {
	@Override
	default Supplier<TownPlanImpl> getNewInstanceSupplier() {
		return TownPlanImpl::new;
	}

	@Override
	default Supplier<TownPlanImpl> getNewInstanceSupplier(EntityId entityId) {
		return () -> new TownPlanImpl(entityId);
	}

	void setRepositoryRoot(@NonNull File repositoryRoot);
}
