package com.innovenso.townplan.broadcast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.innovenso.townplan.api.event.TownPlanEvent;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class TownPlanEventConsoleBroadcaster implements TownPlanBroadcaster {
	private final JsonMapper jsonMapper;

	public TownPlanEventConsoleBroadcaster() {
		jsonMapper = JsonMapper.builder().build();
	}

	@Override
	public void onEvent(TownPlanEvent townPlanEvent) {
		try {
			log.info("An event has occurred: {}", jsonMapper.writeValueAsString(townPlanEvent));
		} catch (JsonProcessingException e) {
			log.error("Could not broadcast event", e);
		}
	}
}
