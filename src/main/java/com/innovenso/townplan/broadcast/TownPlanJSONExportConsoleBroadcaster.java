package com.innovenso.townplan.broadcast;

import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

@Log4j2
public class TownPlanJSONExportConsoleBroadcaster implements TownPlanJSONExportBroadcaster {
	@Override
	public void broadcast(@NonNull File jsonFile) {
		try {
			log.info("JSON export: {}", FileUtils.readFileToString(jsonFile, "UTF-8"));
		} catch (IOException e) {
			log.error("Cannot broadcast JSON export");
		}
	}
}
