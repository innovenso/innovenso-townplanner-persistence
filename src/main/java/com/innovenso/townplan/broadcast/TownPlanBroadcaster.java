package com.innovenso.townplan.broadcast;

import com.innovenso.eventsourcing.api.event.EntityEventListener;
import com.innovenso.townplan.api.event.TownPlanEvent;

public interface TownPlanBroadcaster extends EntityEventListener<TownPlanEvent> {
	public void onEvent(TownPlanEvent townPlanEvent);
}
