package com.innovenso.townplan.broadcast;

import lombok.NonNull;

import java.io.File;

public interface TownPlanJSONExportBroadcaster {
	void broadcast(@NonNull File jsonFile);
}
