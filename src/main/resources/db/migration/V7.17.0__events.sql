CREATE TABLE events (
                        event_id char(36) NOT NULL PRIMARY KEY,
                        entity_id varchar NOT NULL,
                        event_type varchar NOT NULL,
                        created_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
                        event_body text NOT NULL
);

CREATE INDEX events_entity_idx ON events(entity_id);
CREATE INDEX events_type_idx ON events(event_type);