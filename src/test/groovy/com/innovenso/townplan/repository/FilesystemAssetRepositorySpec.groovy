package com.innovenso.townplan.repository

import spock.lang.Specification

import java.nio.file.Path

class FilesystemAssetRepositorySpec extends Specification {
	FileSystemAssetRepository repository

	def setup() {
		repository = new FileSystemAssetRepository("/tmp/townplanner", "https://test.com/files/")
	}

	def "get the path of an object"() {
		when:
		Path path = repository.getPath("mobeja/test.txt")
		then:
		println path
		true
	}

	def "write a file to the repository"() {
		given:
		File tempFile = File.createTempFile("spock", "localFile")
		tempFile << "Hello Local Filesystem"
		when:
		repository.write(tempFile, "mobeja/test.txt")
		then:
		true
	}

	def "read a file from the repository"() {
		when:
		Optional<File> testFile = repository.read("mobeja/test.txt")
		then:
		testFile.isPresent()
		testFile.get().text == "Hello Local Filesystem"
		println testFile.get().text
	}

	def "read file that does not exist"() {
		when:
		Optional<File> testFile = repository.read(UUID.randomUUID().toString())
		then:
		testFile.isEmpty()
	}

	def "list files in the repository"() {
		when:
		List<String> objects = repository.getObjectNames()
		then:
		!objects.isEmpty()
		objects.each {println it }
	}

	def "get CDN URL"() {
		when:
		Optional<String> cdnUrl = repository.getCdnUrl("mobeja/test.txt")
		then:
		cdnUrl.get() == "https://test.com/files/mobeja/test.txt"
	}
}
